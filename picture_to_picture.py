from PIL import Image, ImageDraw
from rgb_xor_text import main
from LSB_check import check_text


def pack_pict_in_pict(path_small_picture: str, path_full_picture: str):
    """
    функция подготавливает картинку которую мы хотим записать в конейнер
    путем получение трех цветов rgb и переводом их в битовую последовательность

    :param path_small_picture: путь к картинке, которую хотим спрятать
    :param path_full_picture: путь к контейнеру
    :return: заполненный стегоконтейнер
    """
    image_small = Image.open(path_small_picture)
    width_small = image_small.size[0]
    height_small = image_small.size[1]
    pix_small = image_small.load()
    mess = []
    for x in range(width_small):
        for y in range(height_small):
            for i in range(3):
                rgb = bin(pix_small[x, y][i])
                rgb_in_mess = list(rgb[2:])
                while len(rgb_in_mess) < 8:
                    rgb_in_mess.insert(0, '0')
                mess.extend(rgb_in_mess)
    main(mess=mess, path_picture=path_full_picture)
    print(len(mess))


def unpack(path_steg_pict: str, width_output_pict: int, height_output_picture: int):
    """
    используя функцию check_text получает спрятанное сообщение
    с помощью двух циклов преобразует последовательность в
    десятичную форму записи для последующей отрисовки

    :param path_steg_pict: директория где лежит стегоконтейнер
    :param width_output_pict: ширина спрятанной картинки
    :param height_output_picture: высота спрятанной картинки
    """
    lenght_output_mess = width_output_pict * height_output_picture * 24
    mess = check_text(path_steg_pict, lenght_output_mess)
    image = Image.new('RGB', (width_output_pict, height_output_picture))
    draw = ImageDraw.Draw(image)
    counter_bytes = 0
    for x in range(width_output_pict):
        for y in range(height_output_picture):
            bytes_r = int(mess[counter_bytes:counter_bytes + 8], 2)
            counter_bytes += 8
            bytes_g = int(mess[counter_bytes:counter_bytes + 8], 2)
            counter_bytes += 8
            bytes_b = int(mess[counter_bytes:counter_bytes + 8], 2)
            counter_bytes += 8
            draw.point((x, y), (bytes_r, bytes_g, bytes_b))
    image.show()

pack_pict_in_pict('pict_s.png', 'kont_S.jpg')
unpack('pict_pict.png', 150,150)
