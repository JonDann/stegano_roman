import wave
from LSB_check import check_text
from rgb_xor_text import main

nchannels = 2
sampwidth = 2
framerate = 48000
nframes = 148608
comptype = 'NONE'
compname = 'not compressed'


def pack(audio_path_wav: str, path_picture: str):
    """
    переводит аудиодорожку в последовательность бит и записывает ее в контейнер

    :param audio_path_wav: путь к аудиодорожке
    :param path_picture: путь к контейнеру
    :return: стегоконтейнер
    """
    # отрываем аудио-файл
    source = wave.open(audio_path_wav, 'rb')
    # получаем количество фремеймов из него
    frames_count = source.getnframes()
    # global frames_count
    # достаём все фреймы
    data = source.readframes(frames_count)
    # переводим в байтовыую строку по 8 бит
    mess = []
    for ch in data:
        new_char = list(bin(ch))[2:]
        while len(new_char) < 8:
            new_char.insert(0, '0')
        mess.extend(new_char)
    main(mess=mess, path_picture=path_picture)
    source.close()
    print(len(mess))
    print(source.getparams())


def unpack(path_picture_steg: str, path_audio_output: str, lenght_output_mess: int):
    """
    извлекает аудиофайл из стегоконтейнера

    :param path_picture_steg: путь к стегоконтейнеру
    :param path_audio_output: путь куда сохранять аудио из стего
    :param lenght_output_mess:
    :return: вытащенный аудиофайл из стегоконтейнера
    """
    # забираем биты из фото
    audio_bit_my = check_text(path=path_picture_steg, length_find=lenght_output_mess)
    data = b''
    for i in range(0, len(audio_bit_my), 8):
        ch_not = int(audio_bit_my[i:i + 8], 2).to_bytes((len(audio_bit_my[i:i + 8])) // 8, 'big')
        data += ch_not
    wav_file = wave.open(path_audio_output, "wb")
    wav_file.setparams((nchannels, sampwidth, framerate, nframes,
                        comptype, compname))
    wav_file.writeframes(data)
    wav_file.close()


pack('audio_S.wav', 'kont_S.jpg')
unpack("audio_posle_xor.png", "123.wav", 4755456)
